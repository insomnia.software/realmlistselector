﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;

namespace RealmlistSelector
{
    class ViewModel : INotifyPropertyChanged
    {
        private string currentRealm;
        public string CurrentRealm { get { return currentRealm; } set { currentRealm = value; OnPropertyChanged("CurrentRealm"); } }

        private string newRealm;
        public string NewRealm { get { return newRealm; } set { newRealm = value; OnPropertyChanged("NewRealm"); } }

        public ObservableCollection<string> realms = new ObservableCollection<string>();
        public ObservableCollection<string> Realms { get { return realms; } set { realms = value; OnPropertyChanged("Realms"); } }

        private string selectedRealm;
        public string SelectedRealm { get { return selectedRealm; } set { selectedRealm = value; OnPropertyChanged("SelectedRealm"); } }

        private ICommand applyCommand;
        public ICommand ApplyCommand
        {
            get { return applyCommand ?? (applyCommand = new RelayCommand(param => Apply())); }
        }

        private void Apply()
        {
            if (string.IsNullOrEmpty(selectedRealm)) return;

            ModifyFile.WriteOutRealmlist(selectedRealm);
            CurrentRealm = ModifyFile.ReadInRealmlist();
        }

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand ?? (addCommand = new RelayCommand(param => Add())); }
        }

        private void Add()
        {
            if (string.IsNullOrEmpty(newRealm)) return;

            ModifyFile.AddRealm(newRealm);
            Realms.Add(newRealm);
        }

        private ICommand removeCommand;
        public ICommand RemoveCommand
        {
            get { return removeCommand ?? (removeCommand = new RelayCommand(param => Remove())); }
        }

        private void Remove()
        {
            ModifyFile.RemoveRealm(selectedRealm);
            Realms.Remove(selectedRealm);
        }

        public ViewModel()
        {
            ModifyFile.LoadRealms();
            foreach (string realm in ModifyFile.GetRealms())
            {
                Realms.Add(realm);
            }

            CurrentRealm = ModifyFile.ReadInRealmlist();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return _canExecute == null ? true : _canExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }

        #endregion // ICommand Members
    }
}
