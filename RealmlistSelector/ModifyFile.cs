﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace RealmlistSelector
{
    public class ModifyFile
    {
        private static List<string> realms = new List<string>();
        private const string FILE_NAME_XML = "C:\\home\\Realmlist.xml";
        private const string FILE_NAME_WTF = "C:\\home\\Realmlist.wtf";

        public static ObservableCollection<string> GetRealms()
        {
            return new ObservableCollection<string>(realms);
        }

        public static void AddRealm(string realm)
        {
            realms.Add(realm);
            SaveRealms();
        }

        public static bool RemoveRealm(string realm)
        {
            if (!realms.Remove(realm)) return false;

            SaveRealms();
            return true;
        }

        public static string ReadInRealmlist()
        {
            string realmString = "";
            try
            {
                realmString = File.ReadAllText(FILE_NAME_WTF);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine(FILE_NAME_WTF + " not found");
            }

            if (Regex.IsMatch(realmString, "set realmlist .+"))
                realmString = realmString.Substring(14);

            return realmString;
        }

        public static void WriteOutRealmlist(string url)
        {
            TextWriter tw = new StreamWriter(FILE_NAME_WTF, false);
            tw.Write("set realmlist " + url);
            tw.Close();
        }

        public static void SaveRealms()
        {
            XmlSerializer xs = new XmlSerializer(typeof(List<string>));
            using (FileStream fs = new FileStream(FILE_NAME_XML, FileMode.Create))
            {
                xs.Serialize(fs, realms);
            }
        }

        public static void LoadRealms()
        {
            try
            {
                using (FileStream fs = new FileStream(FILE_NAME_XML, FileMode.Open))
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(List<string>));
                    realms = (List<string>)(xSer.Deserialize(fs));
                }
            }
            catch (FileNotFoundException)
            {
                //file not found, loading defaults
                //TODO Log this
                //throw new NotImplementedException();
            }

            //realms.Add(ReadInRealmlist());//Add to the list the 
        }
    }
}
